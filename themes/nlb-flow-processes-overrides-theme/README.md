## Getting Started nlb-flow-processes-overrides-theme

To make this theme work without any problems you need to import
nlb-web-retai-theme beacuse this theme overrides it.

For example, you can create merged-themes.scss with content:

```sh
  @import "~@nlb/nlb-web-theme/scss/main";
```

```sh
  @import "nlb-flow-processes-overrides-theme/scss/main";
```
