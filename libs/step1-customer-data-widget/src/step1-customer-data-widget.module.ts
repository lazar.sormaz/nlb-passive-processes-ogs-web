import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step1CustomerDataWidgetComponent } from './step1-customer-data-widget.component';
import { BackbaseUiModule } from '@backbase/ui-ang';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'libs/shared/shared.module';
import { NlbActiveProcessesSharedModuleModule } from '@nlb/nlb-active-processes-shared-module';

@NgModule({
  declarations: [Step1CustomerDataWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step1CustomerDataWidgetComponent },
    }),
    SharedModule,
    BackbaseUiModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NlbActiveProcessesSharedModuleModule,
  ],
})
export class Step1CustomerDataWidgetModule {}
