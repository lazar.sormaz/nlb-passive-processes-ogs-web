import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NlbPaths } from 'libs/shared/nlb-paths';
import { of } from 'rxjs';

@Component({
  selector: 'bb-step1-customer-data-widget',
  templateUrl: './step1-customer-data-widget.component.html',
  styles: [],
})
export class Step1CustomerDataWidgetComponent implements OnInit {
  form: FormGroup;
  formSubmitted = false;
  employmentStatuses = of([
    { id: 1, key: 'EMPLOYED' },
    { id: 2, key: 'SELFEMPLOYED' },
    { id: 3, key: 'UNEMPLOYED' },
    { id: 4, key: 'RETIRED' },
    { id: 5, key: 'STUDENT' },
    { id: 6, key: 'OTHER' },
  ]);
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      employmentStatus: ['', [Validators.required]],
      consent: ['', [Validators.required]],
    });
  }

  get employmentStatus(): AbstractControl {
    return this.form.get('employmentStatus') as AbstractControl;
  }
  get consent(): AbstractControl {
    return this.form.get('consent') as AbstractControl;
  }

  ngOnInit(): void {}

  openLink($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    window.open(NlbPaths.NLB_SLO_DATA_PROCESSING, '_blank');
  }
  //TODO
  back(): void {}

  //TODO
  cancel(): void {}

  //TODO
  next(): void {
    if (this.form.invalid) {
      return;
    }

    this.formSubmitted = true;
  }
}
