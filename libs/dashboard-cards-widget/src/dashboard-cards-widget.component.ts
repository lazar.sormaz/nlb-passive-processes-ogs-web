import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'bb-dashboard-cards-widget',
  templateUrl: './dashboard-cards-widget.component.html',
  styles: [],
})
export class DashboardCardsWidgetComponent implements OnInit {
  @Output() route = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  navigateToRoute() {
    this.route.emit();
  }
}
