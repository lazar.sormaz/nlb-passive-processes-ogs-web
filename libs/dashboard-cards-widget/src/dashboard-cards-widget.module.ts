import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { DashboardCardsWidgetComponent } from './dashboard-cards-widget.component';
import { HeaderModule } from '@backbase/ui-ang';

@NgModule({
  declarations: [DashboardCardsWidgetComponent],
  imports: [
    CommonModule,
    HeaderModule,
    BackbaseCoreModule.withConfig({
      classMap: { DashboardCardsWidgetComponent },
    }),
  ],
})
export class DashboardCardsWidgetModule {}
