import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step2GradualSavingsDataWidgetComponent } from './step2-gradual-savings-data-widget.component';
import { NlbActiveProcessesSharedModuleModule } from '@nlb/nlb-active-processes-shared-module';
import { BackbaseUiModule } from '@backbase/ui-ang';
import { SharedModule } from 'libs/shared/shared.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelpersService, NlbSharedModule } from '@nlb/nlb-shared-module';

@NgModule({
  declarations: [Step2GradualSavingsDataWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step2GradualSavingsDataWidgetComponent },
    }),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    BackbaseUiModule,
    NgxSliderModule,
    NlbSharedModule,
    NlbActiveProcessesSharedModuleModule,
  ],
  providers: [HelpersService],
})
export class Step2GradualSavingsDataWidgetModule {}
