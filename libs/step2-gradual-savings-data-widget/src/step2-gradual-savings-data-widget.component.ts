import { Options } from '@angular-slider/ngx-slider';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'bb-step2-gradual-savings-data-widget',
  templateUrl: './step2-gradual-savings-data-widget.component.html',
  styles: [],
})
export class Step2GradualSavingsDataWidgetComponent implements OnInit {
  @Input() currencies: any;
  validationRegex: any;
  monthlyExpense = 10;
  options: Options = {
    floor: 0,
    ceil: 120,
    showSelectionBar: true,
  };

  gradualSavingsForm: FormGroup | any;

  constructor(private fb: FormBuilder) {
    this.gradualSavingsForm = this.fb.group({
      amount: ['100', [Validators.required]],
      currency: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}

  get amountCurrencyInvalid(): boolean {
    return (
      (this.gradualSavingsForm ||
        this.gradualSavingsForm.get('amount').dirty ||
        this.gradualSavingsForm.get('currency').dirty) &&
      (this.gradualSavingsForm.get('amount').invalid || this.gradualSavingsForm.get('currency').invalid)
    );
  }

  //TODO
  back(): void {}

  //TODO
  cancel(): void {}

  //TODO
  next(): void {}
}
