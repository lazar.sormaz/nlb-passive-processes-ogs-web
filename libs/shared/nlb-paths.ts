export enum NlbPaths {
  NLB_SLO = 'https://www.nlb.si/',
  NLB_SLO_DATA_PROCESSING = 'https://www.nlb.si/varstvo-osebnih-podatkov',
}
