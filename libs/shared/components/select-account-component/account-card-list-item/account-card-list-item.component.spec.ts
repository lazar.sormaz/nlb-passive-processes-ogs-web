import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCardListItemComponent } from './account-card-list-item.component';

describe('AccountCardListItemComponent', () => {
  let component: AccountCardListItemComponent;
  let fixture: ComponentFixture<AccountCardListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountCardListItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCardListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
