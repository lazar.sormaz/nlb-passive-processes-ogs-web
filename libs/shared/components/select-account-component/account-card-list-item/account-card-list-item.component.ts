import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked } from '@angular/core';
import { get } from 'lodash';

@Component({
  selector: 'nlb-account-card-list',
  templateUrl: './account-card-list-item.component.html',
  styleUrls: ['./account-card-list-item.component.scss'],
})
export class AccountCardListComponent implements OnInit, AfterContentChecked {
  @Input() account: any;
  @Input() showShadow = true;
  @Input() showHover = true;
  @Input() smallerCard: boolean | undefined;
  @Input() includeMargin = true;
  @Input() showBorderBottom: boolean | undefined;
  @Input() selectedAccount: any;
  @Input() showChevronIcon = false;
  @Input() showIBAN = true;
  @Input() showRemoveOption = false;
  @Output() removeOption: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    // this.account.productKindNameTemp = 'savingsAccount';
    // this.account.productKindNameTemp = this.account.productKindName ? camelCase(this.account.productKindName):'';
    // this.setUniqueAmountField();
  }

  ngAfterContentChecked() {
    // this.account.productKindNameTemp = this.account.productKindName ? camelCase(this.account.productKindName):'';
    // this.setUniqueAmountField();
  }

  setUniqueAmountField() {
    if (
      this.account.productKindNameTemp === 'currentAccount' ||
      this.account.productKindNameTemp === 'creditCard' ||
      this.account.productKindNameTemp === 'debitCard'
    ) {
      this.account.amount = get(this.account, 'additions.currentBalance') || this.account.availableBalance;
    } else if (this.account.productKindNameTemp === 'savingsAccount') {
      this.account.amount = get(this.account, 'additions.currentBalance') || this.account.bookedBalance;
    } else if (this.account.productKindNameTemp === 'investmentAccount') {
      this.account.amount = get(this.account, 'additions.currentBalance') || this.account.currentInvestmentValue;
    } else if (this.account.productKindNameTemp === 'loan') {
      this.account.amount = get(this.account, 'additions.currentBalance') || this.account.principalAmount;
    } else if (this.account.productKindNameTemp === 'termDeposit' || this.account.productKindNameTemp === 'savings') {
      this.account.amount = get(this.account, 'additions.currentBalance') || '';
    } else {
      this.account.amount = get(this.account, 'additions.currentBalance') || '';
    }
  }

  unselectOption(option: any) {
    this.removeOption.emit(option);
  }
}
