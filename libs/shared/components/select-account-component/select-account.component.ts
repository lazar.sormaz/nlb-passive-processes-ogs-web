import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef,
} from '@angular/core';

@Component({
  selector: 'nlb-select-account-component',
  templateUrl: './select-account.component.html',
  styleUrls: ['./select-account.component.scss'],
})
export class SelectAccountComponent implements OnInit {
  @Input() options?: Array<any>;
  @Input() selectedAccount: any;
  @Output() insideClick: EventEmitter<any> = new EventEmitter();
  @Output() optionSelected: EventEmitter<any> = new EventEmitter();

  option: any = {};

  selectOpen = false;
  constructor(private _elementRef: ElementRef, private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.insideClick.subscribe((event: any) => {
      this.onOutsideClick(event.target);
    });
  }

  toggleSelect() {
    this.selectOpen = !this.selectOpen;
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  @HostListener('document:click', ['$event.target'])
  onOutsideClick(targetElement: any) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.selectOpen = false;
    }
  }

  selectItem(option: any) {
    this.selectedAccount = option;
    this.optionSelected.emit(option);
    this.cd.detectChanges();
    this.toggleSelect();
  }
}
