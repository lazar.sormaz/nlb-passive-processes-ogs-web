import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseUiModule, HeaderModule, IconModule, ModalModule, ProgressbarModule } from '@backbase/ui-ang';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [],
  imports: [CommonModule, ProgressbarModule, IconModule, HeaderModule, ModalModule, BackbaseUiModule, NgSelectModule],
  exports: [NgSelectModule],
  providers: [],
  entryComponents: [],
})
export class SharedModule {}
