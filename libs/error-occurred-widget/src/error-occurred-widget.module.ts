import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { ErrorOccurredWidgetComponent } from './error-occurred-widget.component';

@NgModule({
  declarations: [ErrorOccurredWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { ErrorOccurredWidgetComponent },
    }),
  ],
})
export class ErrorOccurredWidgetModule {}
