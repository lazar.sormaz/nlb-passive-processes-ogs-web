import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step4SigningDataWidgetComponent } from './step4-signing-data-widget.component';

@NgModule({
  declarations: [Step4SigningDataWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step4SigningDataWidgetComponent },
    }),
  ],
})
export class Step4SigningDataWidgetModule {}
