import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step6FinalizeDataWidgetComponent } from './step6-finalize-data-widget.component';

@NgModule({
  declarations: [Step6FinalizeDataWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step6FinalizeDataWidgetComponent },
    }),
  ],
})
export class Step6FinalizeDataWidgetModule {}
