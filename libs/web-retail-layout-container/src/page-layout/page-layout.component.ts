import { Component, Input, ElementRef, ViewChild } from '@angular/core';

import { Observable } from 'rxjs';

import { ItemModelTree, AssetsService, AssetsResolver } from '@backbase/foundation-ang/core';

@Component({
  selector: 'nlb-web-retail-page-layout-ui',
  templateUrl: './page-layout.component.html',
})
export class PageLayoutComponent {
  hostRef: PageLayoutComponent = this;
  /**
   * Receives the children to be rendered. [Required]
   */
  @Input() children: ItemModelTree | any;

  /**
   * Receives the sidebar column classes to add some custom styles to it. [Optional]
   */
  @Input() sidebarColumnClasses: string | any;

  /**
   * Receives the content column classes to add some custom styles to it. [Optional]
   */
  @Input() contentColumnClasses: string | any;

  /**
   * Receives the topbar classes to add some custom styles to it. [Optional]
   */
  @Input() topBarColumnClasses: string | any;

  /**
   * Receives the menu icon classes. Required because no icon will be shown if not provided. [Required]
   */
  @Input() menuButtonIcon: string | any;

  /**
   * Set up a reference to sidebar DOM element.
   */
  @ViewChild('sidebar', { read: ElementRef, static: false })
  sidebar: ElementRef | any;

  /**
   * Set up a reference to main content DOM element.
   */
  @ViewChild('mainContent', { read: ElementRef, static: false })
  mainContent: ElementRef | any;

  /**
   * Set up a reference to whole content DOM element.
   */
  @ViewChild('content', { read: ElementRef, static: false })
  content: ElementRef | any;

  constructor(private assetService: AssetsService) {}

  resolveAsset: AssetsResolver = this.assetService.createAssetsResolver({
    widgetName: 'nlb-web-retail-layout-container-ang',
    getURIFromAssetID: (assetId: string) => assetId,
  });

  // logoLg: Observable<string> = this.resolveAsset("nlb-logo_full.svg");
  // logoSm: Observable<string> = this.resolveAsset("nlb-logo_small.svg");

  /**
   * On executing this function sidebar adds or removes toggle class.
   * Regarding if the class is added or removed it shows/hide/shrink the sidebar to
   * give more space to the user or to get easier access to the menu options.
   */
  toggleSidebar() {
    const sidebar = <ElementRef>this.sidebar;
    sidebar.nativeElement.classList.toggle('toggle');

    const content = <ElementRef>this.content;
    content.nativeElement.classList.toggle('toggle');
  }

  /**
   * This is an accesibility improvement used by "skip links".
   * It focus the main content section so an user with a screen reader can go
   * directly to the content.
   */
  focusElement(): void {
    const mainContent = <ElementRef>this.mainContent;
    mainContent.nativeElement.focus();
  }

  closeSidebar() {
    const sidebar = <ElementRef>this.sidebar;
    sidebar.nativeElement.classList.remove('toggle');

    const content = <ElementRef>this.content;
    content.nativeElement.classList.remove('toggle');
  }
}
