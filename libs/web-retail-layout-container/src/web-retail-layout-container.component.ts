import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemModel, ItemModelTree } from '@backbase/foundation-ang/core';

@Component({
  selector: 'nlb-web-retail-layout-container',
  templateUrl: 'web-retail-layout-container.component.html',
  styles: [],
})
export class WebRetailLayoutContainerComponent {
  /**
   * Self reference to be used in customizable slots.
   */
  readonly hostRef: WebRetailLayoutContainerComponent = this;

  /**
   * Stores the sidebar column classes to add some custom styles to it, from properties in model.
   */
  readonly sidebarColumnClasses: Observable<string | undefined> = this.model.property<string>('sidebarColumnClasses');

  /**
   * Stores the sidebar column classes to add some custom styles to it, from properties in model.
   */
  readonly topBarColumnClasses: Observable<string | undefined> = this.model.property<string>('topBarColumnClasses');

  /**
   * Stores the content column classes to add some custom styles to it, from properties in model.
   */
  readonly contentColumnClasses: Observable<string | undefined> = this.model.property<string>('contentColumnClasses');

  /**
   * Stores the menu button icon classes. CXP editable menu icon.
   */
  readonly menuButtonIcon: Observable<string | undefined> = this.model.property<string>('menuButtonIcon');

  /**
   * Stores the sidebar column classes to add some custom styles to it.
   */
  readonly children: Observable<Array<ItemModelTree>> = this.itemModelTree.children;

  /**
   * Injecting ItemModel and ItemModelTree to setup the properties of the component.
   */
  constructor(private model: ItemModel, private itemModelTree: ItemModelTree) {}
}
