import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BackbaseCoreModule } from '@backbase/foundation-ang/core';

import { WebRetailLayoutContainerComponent } from './web-retail-layout-container.component';

import { PageLayoutComponent } from './page-layout/page-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BackbaseCoreModule.withConfig({
      classMap: {
        WebRetailLayoutContainerComponent,
      },
    }),
  ],
  declarations: [WebRetailLayoutContainerComponent, PageLayoutComponent],
})
export class WebRetailLayoutContainerModule {}
