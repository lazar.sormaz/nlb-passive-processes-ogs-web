import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step3StandingOrderDataWidgetComponent } from './step3-standing-order-data-widget.component';
import { NlbActiveProcessesSharedModuleModule } from '@nlb/nlb-active-processes-shared-module';
import { BackbaseUiModule } from '@backbase/ui-ang';
import { SharedModule } from 'libs/shared/shared.module';
import { NlbSharedModule } from '@nlb/nlb-shared-module';
import { SelectAccountComponent } from 'libs/shared/components/select-account-component/select-account.component';
import { AccountCardListComponent } from 'libs/shared/components/select-account-component/account-card-list-item/account-card-list-item.component';
import { UiComponentsLibModule } from '@nlb/nlb-ui-components-ang';

@NgModule({
  declarations: [Step3StandingOrderDataWidgetComponent, SelectAccountComponent, AccountCardListComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step3StandingOrderDataWidgetComponent },
    }),
    NlbActiveProcessesSharedModuleModule,
    BackbaseUiModule,
    SharedModule,
    NlbSharedModule,
    UiComponentsLibModule,
  ],
})
export class Step3StandingOrderDataWidgetModule {}
