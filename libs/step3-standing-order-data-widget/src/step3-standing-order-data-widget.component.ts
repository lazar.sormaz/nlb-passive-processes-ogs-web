import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bb-step3-standing-order-data-widget',
  templateUrl: './step3-standing-order-data-widget.component.html',
  styles: [],
})
export class Step3StandingOrderDataWidgetComponent implements OnInit {
  myAccounts: any;
  currentSelectedAccount: any;
  public currentAccounts: Array<any> = [];
  public selectedAccount: any = {};
  showSelector = true;
  checklist: any[] = [];

  constructor() {
    this.checklist = [
      { id: 1, value: '8th of month', isSelected: true },
      { id: 2, value: '18th of month', isSelected: false },
      { id: 3, value: '28th of month', isSelected: false },
    ];
  }

  ngOnInit(): void {}

  onSelectAccount(acc: any): void {
    this.selectedAccount = acc;
    // this.accountService.setSelectedAccount(acc);
  }

  isAllSelected(item: any) {
    this.checklist.forEach((val) => {
      if (val.id === item.id) {
        val.isSelected = !val.isSelected;
      } else {
        val.isSelected = false;
      }
    });
  }

  selectAccount(account: any) {
    if (account) {
      this.selectedAccount.emit(account.id);
    }
  }
  //TODO
  back(): void {}

  //TODO
  cancel(): void {}

  //TODO
  next(): void {}
}
