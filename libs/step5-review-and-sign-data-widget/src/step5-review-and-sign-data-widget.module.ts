import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { Step5ReviewAndSignDataWidgetComponent } from './step5-review-and-sign-data-widget.component';

@NgModule({
  declarations: [Step5ReviewAndSignDataWidgetComponent],
  imports: [
    CommonModule,
    BackbaseCoreModule.withConfig({
      classMap: { Step5ReviewAndSignDataWidgetComponent },
    }),
  ],
})
export class Step5ReviewAndSignDataWidgetModule {}
