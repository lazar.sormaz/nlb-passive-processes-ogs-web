import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BackbaseCoreModule } from '@backbase/foundation-ang/core';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { WebRetailLayoutContainerModule } from '@@nlb/web-retail-layout-container';
import { ErrorOccurredWidgetModule } from '@@nlb/error-occurred-widget';
import { NavigationSpaWidgetModule } from '@backbase/universal-ang/navigation';
import { HeadingWidgetModule } from '@backbase/universal-ang/heading';
import { ContentWidgetModule } from '@backbase/universal-ang/content';
import { DashboardCardsWidgetModule } from '@@nlb/dashboard-cards-widget';
import { FlowInteractionContainerModule, FlowInteractionCoreModule } from '@backbase/flow-interaction-sdk-ang/core';
import { NlbActiveProcessesSharedModuleModule } from '@nlb/nlb-active-processes-shared-module';
import { NlbSharedModule } from '@nlb/nlb-shared-module';

import {
  ContainersModule,
  PanelContainerModule,
  DeckContainerModule,
  TabContainerModule,
} from '@backbase/universal-ang/containers';

import { Step1CustomerDataWidgetModule } from '@nlb/step1-customer-data-widget';
import { SharedModule } from 'libs/shared/shared.module';
import { Step2GradualSavingsDataWidgetModule } from '@nlb/step2-gradual-savings-data-widget';
import { Step3StandingOrderDataWidgetModule } from '@nlb/step3-standing-order-data-widget';
import { Step4SigningDataWidgetModule } from '@nlb/step4-signing-data-widget';
import { Step5ReviewAndSignDataWidgetModule } from '@nlb/step5-review-and-sign-data-widget';
import { Step6FinalizeDataWidgetModule } from '@nlb/step6-finalize-data-widget';
import { UiComponentsLibModule } from '@nlb/nlb-ui-components-ang';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    BackbaseCoreModule.forRoot({
      features: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        THEME_V2: true,
      },
    }),
    HttpClientModule,
    NlbActiveProcessesSharedModuleModule,
    NavigationSpaWidgetModule,
    HeadingWidgetModule,
    ContentWidgetModule,
    UiComponentsLibModule,
    NlbSharedModule,
    FlowInteractionContainerModule,
    FlowInteractionCoreModule.forRoot({
      actionConfig: {
        'customer-data': { updateCdo: true },
        'gradual-savings-data': { updateCdo: true },
        'standing-order': { updateCdo: true },
        'signing-info': { updateCdo: true },
        'review-and-sign': { updateCdo: true },
        finalize: { updateCdo: true },
      },
      steps: {
        'customer-data': ['ogs/process/customer-data'],
        'gradual-savings-data': ['ogs/process/gradual-savings-data'],
        'standing-order': ['ogs/process/standing-order'],
        'signing-info': ['ogs/process/signing-info'],
        'review-and-sign': ['ogs/process/review-and-sign'],
        finalize: ['ogs/process/finalize'],
      },
    }),
    ContainersModule,
    PanelContainerModule,
    DeckContainerModule,
    DashboardCardsWidgetModule,
    TabContainerModule,
    BackbaseCoreModule,
    WebRetailLayoutContainerModule,
    RouterModule.forRoot([], { initialNavigation: 'disabled', useHash: true }),
    environment.animation ? BrowserAnimationsModule : NoopAnimationsModule,
    ErrorOccurredWidgetModule,
    Step1CustomerDataWidgetModule,
    SharedModule,
    Step2GradualSavingsDataWidgetModule,
    Step3StandingOrderDataWidgetModule,
    Step4SigningDataWidgetModule,
    Step5ReviewAndSignDataWidgetModule,
    Step6FinalizeDataWidgetModule,
  ],
  providers: [...(environment.mockProviders || [])],
  bootstrap: [AppComponent],
})
export class AppModule {}
