import { createMocksInterceptor } from '@backbase/foundation-ang/data-http';

import { ExternalServices } from '@backbase/foundation-ang/start';

import { Environment } from './type';

const services: ExternalServices = {};

const pageModel: any = {
  name: 'nlb-web-ui-ang',
  properties: {},
  children: [
    {
      name: 'nlb-web-retail-layout-container-ang-_-e9f144c34295',
      properties: {
        area: '0',
        'child.4.title': 'Main Content Area',
        src: '$(contextRoot)/static/items/nlb-web-retail-layout-container-ang/index.html',
        'child.amountToCreate': '5',
        'child.0.title': 'Sidebar Area',
        'child.3.title': 'Topbar Area Right',
        label: 'NLB WebRetail Layout Flex Universal Container',
        title: 'NLB WebRetail Layout Flex Universal Container',
        menuButtonIcon: 'icon-ico-push-3',
        'child.2.title': 'Topbar Area Center',
        classId: 'WebRetailLayoutContainerComponent',
        'child.extendedItemName': 'bb-panel-container-ang',
        'render.requires': 'render-bb-widget-3',
        'child.1.title': 'Topbar Area Left',
        order: '0',
      },
      children: [
        {
          name: 'bb-panel-container-ang-_-0e78c13e705d',
          properties: {
            area: '0',
            classId: 'PanelContainerComponent',
            appearance: 'none',
            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
            'render.requires': 'render-bb-widget-3',
            cssClasses: 'd-block mt-5',
            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
            title: 'Sidebar Area',
            order: '8',
          },
          children: [
            {
              name: 'bb-navigation-spa-widget-ang-_-e80c3e732b54',
              properties: {
                area: '0',
                src: '$(contextRoot)/static/items/bb-navigation-spa-widget-ang/index.html',
                label: 'Universal Navigation SPA Widget',
                title: 'Universal Navigation SPA Widget',
                isSticky: 'false',
                noOfVisibleLinks: '3',
                displayDropdown: 'true',
                classId: 'NavigationSpaWidgetComponent',
                'render.requires': 'render-bb-widget-3',
                options: '$(contextRoot)/static/items/bb-navigation-spa-widget-ang/options.json',
                navigationType: 'vertical',
                sourceRoutesName: 'bb-deck-container-ang-_-07078e67d92d',
                order: '8',
              },
            },
          ],
        },
        {
          name: 'bb-panel-container-ang-_-939d3a737502',
          properties: {
            area: '0',
            classId: 'PanelContainerComponent',
            appearance: 'none',
            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
            'render.requires': 'render-bb-widget-3',
            cssClasses: 'd-block',
            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
            title: 'Topbar Area Left',
            order: '16',
          },
          children: [],
        },
        {
          name: 'bb-panel-container-ang-_-edf9db2b2286',
          properties: {
            area: '0',
            classId: 'PanelContainerComponent',
            appearance: 'none',
            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
            'render.requires': 'render-bb-widget-3',
            cssClasses: 'd-block',
            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
            title: 'Topbar Area Center',
            order: '24',
          },
          children: [],
        },
        {
          name: 'bb-panel-container-ang-_-d39b8e2c8f49',
          properties: {
            area: '0',
            classId: 'PanelContainerComponent',
            appearance: 'none',
            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
            'render.requires': 'render-bb-widget-3',
            cssClasses: 'd-block',
            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
            title: 'Topbar Area Right',
            order: '32',
          },
          children: [],
        },
        {
          name: 'bb-panel-container-ang-_-4e4110fd0e71',
          properties: {
            area: '0',
            classId: 'PanelContainerComponent',
            appearance: 'none',
            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
            'render.requires': 'render-bb-widget-3',
            cssClasses: 'd-block main-container mx-auto mt-5',
            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
            title: 'Main Content Area',
            order: '40',
          },
          children: [
            {
              name: 'bb-deck-container-ang-_-07078e67d92d',
              properties: {
                area: '0',
                src: '$(contextRoot)/static/items/bb-deck-container-ang/index.html',
                'child.amountToCreate': '3',
                'child.0.title': 'Deck 1',
                title: 'Deck shown in navigation',
                disableDefault: 'false',
                'child.2.title': 'Deck 3',
                classId: 'DeckContainerComponent',
                'child.extendedItemName': 'bb-panel-container-ang',
                'render.requires': 'render-bb-widget-3',
                'child.1.title': 'Deck 2',
                order: '8',
              },
              children: [
                {
                  name: 'bb-panel-container-ang-_-40ca8eb6b532',
                  properties: {
                    area: '0',
                    classId: 'PanelContainerComponent',
                    appearance: 'none',
                    route: 'ogs',
                    src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
                    'render.requires': 'render-bb-widget-3',
                    cssClasses: 'd-block',
                    options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
                    title: 'OGS',
                    order: '8',
                    navIcon: 'x icon-nlb-ico-loan',
                  },
                  children: [
                    {
                      id: '1c3dc08c-ef50-4191-aba7-4ab85a17528b',
                      name: 'bb-deck-container-ang-_-3cdb781d6b3b',
                      properties: {
                        area: '0',
                        src: '$(contextRoot)/static/items/bb-deck-container-ang/index.html',
                        'child.amountToCreate': '3',
                        'child.0.title': 'Deck 1',
                        title: 'My Product Deck Container',
                        disableDefault: 'false',
                        'child.2.title': 'Deck 3',
                        classId: 'DeckContainerComponent',
                        'child.extendedItemName': 'bb-panel-container-ang',
                        'render.requires': 'render-bb-widget-3',
                        'child.1.title': 'Deck 2',
                        order: '8',
                      },
                      children: [
                        {
                          id: 'c9ad22c4-69b2-43dd-9f98-83b25b7bdddc',
                          name: 'bb-panel-container-ang-_-e4cbbe24f8f0',
                          properties: {
                            area: '0',
                            classId: 'PanelContainerComponent',
                            appearance: 'none',
                            route: 'overview',
                            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
                            'render.requires': 'render-bb-widget-3',
                            cssClasses: 'd-block',
                            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
                            title: 'Product List',
                            order: '2',
                          },
                          children: [
                            {
                              name: 'bb-heading-widget-ang_3c847c91-f3c6-4662-8829-c1a39bceae7e-_-146c74209993',
                              properties: {
                                headingType: 'h5',
                                area: '0',
                                heading: 'Account',
                                src: '$(contextRoot)/static/items/bb-heading-widget-ang/index.html',
                                'output.buttonAction': 'navigation:',
                                useButtonActionEventBus: 'false',
                                title: 'Universal Heading Widget',
                                classId: 'HeadingWidgetComponent',
                                'render.requires': 'render-bb-widget-3',
                                options: '$(contextRoot)/static/items/bb-heading-widget-ang/options.json',
                                buttonClasses: 'btn btn-link',
                                order: '1',
                              },
                            },
                            {
                              name: 'dashboard-cards-widget',
                              properties: {
                                classId: 'DashboardCardsWidgetComponent',
                                'output.route': 'navigation:bb-panel-container-ang-_-e4cbbe24f8f0123',
                              },
                            },
                          ],
                        },
                        {
                          id: 'c9ad22c4-69b2-43dd-9f98-83b25b7bdddc123',
                          name: 'bb-panel-container-ang-_-e4cbbe24f8f0123',
                          properties: {
                            area: '0',
                            classId: 'PanelContainerComponent',
                            appearance: 'none',
                            route: 'process',
                            src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
                            'render.requires': 'render-bb-widget-3',
                            cssClasses: 'd-block',
                            options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
                            title: 'Product List',
                            order: '2',
                          },
                          children: [
                            {
                              name: 'ono-reference-flow',
                              properties: {
                                classId: 'FlowInteractionContainerComponent',
                                apiVersion: 'v1',
                                'input.apiVersion': 'model apiVersion',
                                interactionName: 'ogs-slo',
                                'input.interactionName': 'model interactionName',
                                servicePath: 'nlb-slo-nco-service/api/interaction',
                                'input.servicePath': 'model servicePath',
                                deploymentPath: 'interactions',
                                'input.deploymentPath': 'model deploymentPath',
                                journeyName: 'onboarding',
                                'input.journeyName': 'model journeyName',
                              },
                              children: [
                                {
                                  name: 'step1-customer-data-widget',
                                  properties: {
                                    classId: 'Step1CustomerDataWidgetComponent',
                                    route: 'customer-data',
                                  },
                                },
                                {
                                  name: 'step2-gradual-savings-data-widget',
                                  properties: {
                                    classId: 'Step2GradualSavingsDataWidgetComponent',
                                    route: 'gradual-savings-data',
                                  },
                                },
                                {
                                  name: 'step3-standing-order-data-widget',
                                  properties: {
                                    classId: 'Step3StandingOrderDataWidgetComponent',
                                    route: 'standing-order',
                                  },
                                },
                                {
                                  name: 'step4-signing-data-widget',
                                  properties: {
                                    classId: 'Step4SigningDataWidgetComponent',
                                    route: 'signing-info',
                                  },
                                },
                                {
                                  name: 'step5-review-and-sign-data-widget',
                                  properties: {
                                    classId: 'Step5ReviewAndSignDataWidgetComponent',
                                    route: 'review-and-sign',
                                  },
                                },
                                {
                                  name: 'step6-finalize-data-widget',
                                  properties: {
                                    classId: 'Step6FinalizeDataWidgetComponent',
                                    route: 'finalize',
                                  },
                                },
                                {
                                  name: 'error-occurred-widget',
                                  properties: {
                                    classId: 'ErrorOccurredWidgetComponent',
                                    route: 'error-occurred',
                                  },
                                },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
                {
                  name: 'bb-panel-container-ang-_-d4c4d3504280',
                  properties: {
                    area: '0',
                    classId: 'PanelContainerComponent',
                    appearance: 'none',
                    route: 'my-analytics',
                    src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
                    'render.requires': 'render-bb-widget-3',
                    cssClasses: 'd-block',
                    options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
                    title: 'My analytics',
                    order: '16',
                    navIcon: 'x icon-nlb-ico-loan',
                  },
                  children: [
                    {
                      name: 'bb-heading-widget-ang_3c847c91-f3c6-4662-8829-c1a39bceae7e-_-729410a645bf',
                      properties: {
                        headingType: 'h5',
                        area: '0',
                        heading: 'My analytics',
                        src: '$(contextRoot)/static/items/bb-heading-widget-ang/index.html',
                        'output.buttonAction': 'navigation:',
                        useButtonActionEventBus: 'false',
                        title: 'Universal Heading Widget',
                        classId: 'HeadingWidgetComponent',
                        'render.requires': 'render-bb-widget-3',
                        options: '$(contextRoot)/static/items/bb-heading-widget-ang/options.json',
                        buttonClasses: 'btn btn-link',
                        order: '8',
                      },
                    },
                  ],
                },
                {
                  name: 'bb-panel-container-ang-_-0ba0b94e8134',
                  properties: {
                    area: '0',
                    classId: 'PanelContainerComponent',
                    appearance: 'none',
                    route: 'store-analytics',
                    src: '$(contextRoot)/static/items/bb-panel-container-ang/index.html',
                    'render.requires': 'render-bb-widget-3',
                    cssClasses: 'd-block',
                    options: '$(contextRoot)/static/items/bb-panel-container-ang/options.json',
                    title: 'Store analytics',
                    order: '24',
                    navIcon: 'x icon-nlb-ico-loan',
                  },
                  children: [
                    {
                      name: 'bb-heading-widget-ang_3c847c91-f3c6-4662-8829-c1a39bceae7e-_-15378ec03da0',
                      properties: {
                        headingType: 'h5',
                        area: '0',
                        heading: 'Store analytics',
                        src: '$(contextRoot)/static/items/bb-heading-widget-ang/index.html',
                        'output.buttonAction': 'navigation:',
                        useButtonActionEventBus: 'false',
                        title: 'Universal Heading Widget',
                        classId: 'HeadingWidgetComponent',
                        'render.requires': 'render-bb-widget-3',
                        options: '$(contextRoot)/static/items/bb-heading-widget-ang/options.json',
                        buttonClasses: 'btn btn-link',
                        order: '8',
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};

export const environment: Environment = {
  production: false,
  mockProviders: [createMocksInterceptor()],
  bootstrap: {
    pageModel,
    services,
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
